<html>
<head>
	<title>Calificaciones</title>
</head>
<body>
	<h2>Calificaciones</h2>
	<form method="POST">
		<label>Ingrese la primera nota:</label>
		<input type="number" name="nota1"><br>
		<label>Ingrese la segunda nota:</label>
		<input type="number" name="nota2"><br>
		<label>Ingrese la tercera nota:</label>
		<input type="number" name="nota3"><br>
		<label>Ingrese la cuarta nota:</label>
		<input type="number" name="nota4"><br>
		<input type="submit" value="Calcular promedio">
	</form>
	<br>
	<?php
		if(isset($_POST['nota1']) && isset($_POST['nota2']) && isset($_POST['nota3']) && isset($_POST['nota4'])) {
			$notas = array($_POST['nota1'], $_POST['nota2'], $_POST['nota3'], $_POST['nota4']);
			rsort($notas);
			$promedio = ($notas[0] + $notas[1] + $notas[2]) / 3;
			if($promedio >= 11) {
				echo "Aprobado";
			} else {
				echo "Desaprobado";
			}
		}
	?>
</body>
</html>
