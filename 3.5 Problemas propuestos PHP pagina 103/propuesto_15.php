<!DOCTYPE html>
<html>
<head>
	<title>Ordenar numeros en acendente y descendente </title>
    <style type="text/css">
<!--
.TextoFondo {
background-color: #CCFFFF;
}
body{
    display: flex;
    align-items: center;
    color: blueviolet;
    justify-content: center;
}
body{
    background-color: aquamarine;
}

-->
</style>
</head>
<body>
	<form method="post">
		<label for="num1">ingrese el numero 1:</label>
		<input type="number" name="num1" id="num1"><br>
		<label for="num2">ingrese el numero 2:</label>
		<input type="number" name="num2" id="num2"><br>
		<label for="num3">ingrese el numero 3:</label>
		<input type="number" name="num3" id="num3"><br>
		<input type="submit" name="submit" value="Ordenar">
	</form>
	<?php
	if (isset($_POST['submit'])) {
		$num1 = $_POST['num1'];
		$num2 = $_POST['num2'];
		$num3 = $_POST['num3'];
		$numeros = array($num1, $num2, $num3);
		sort($numeros); 
		echo "Ascendente: " . implode(", ", $numeros) . "<br>"; 
		rsort($numeros);
		echo "Descendente: " . implode(", ", $numeros); 
	}
	?>

</body>
</html>