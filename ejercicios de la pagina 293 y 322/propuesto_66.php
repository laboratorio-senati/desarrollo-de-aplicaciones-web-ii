<html>
<head>
	<title>Verificar Palindromo</title>
</head>
<body>
	<h1>Verificar Palindromo</h1>
	<form method="post" action="propuesto_66.php">
		<label for="palabra">Palabra:</label>
		<input type="text" name="palabra" id="palabra">
		<input type="submit" value="Verificar">
	</form>
</body>
</html>
<?php
$palabra = "ANA";
$palabra_invertida = strrev($palabra);

if ($palabra == $palabra_invertida) {
    echo "La palabra es un palindromo";
} else {
    echo "La palabra no es un palindromo";
}
?>
