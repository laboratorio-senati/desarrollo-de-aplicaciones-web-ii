
<html>
<head>
	<title>Multiplicar y sumar matriz</title>
</head>
<body>

	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<label for="num1">numero 1:</label>
		<input type="number" name="num1" id="num1"><br>

		<label for="num2">numero 2:</label>
		<input type="number" name="num2" id="num2"><br>

		<label for="num3">numero 3:</label>
		<input type="number" name="num3" id="num3"><br>

		<label for="num4">numero 4:</label>
		<input type="number" name="num4" id="num4"><br>

		<label for="num5">numero 5:</label>
		<input type="number" name="num5" id="num5"><br>

		<label for="num6">numero 6:</label>
		<input type="number" name="num6" id="num6"><br>

		<label for="k">Valor de K:</label>
		<input type="number" name="k" id="k"><br>

		<input type="submit" name="submit" value="Calcular">
	</form>

	<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$matriz = array(array($_POST["num1"], $_POST["num2"], $_POST["num3"]), array($_POST["num4"], $_POST["num5"], $_POST["num6"]));

		$k = $_POST["k"];
		foreach ($matriz as &$fila) {
			foreach ($fila as &$elemento) {
				$elemento *= $k;
			}
		}

		$suma = 0;
		foreach ($matriz as $fila) {
			foreach ($fila as $elemento) {
				$suma += $elemento;
			}
		}

		echo "<p>La matriz multiplicada por " . $k . " y sumada es: " . $suma . "</p>";
	}
	?>

</body>
</html>
