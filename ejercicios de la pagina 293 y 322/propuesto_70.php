
<!DOCTYPE html>
<html>
<head>
	<title>Desencriptar</title>
</head>
<body>
	<h1>Desencriptar Frase</h1>
	<form method="post" action="propuesto_70.php">
		<label for="frase_encriptada">Frase encriptada:</label>
		<input type="text" name="frase_encriptada" id="frase_encriptada">
        <br><br>
		<label for="clave">Clave:</label>
		<input type="number" name="clave" id="clave" min="0">
        <br><br>
		<input type="submit" value="Desencriptar">
        <br><br>
	</form>
</body>
</html>

<?php
$frase_encriptada = "ifmmp!ebub!/!";

function desencriptar($frase_encriptada, $clave) {
    $frase_desencriptada = "";
    $longitud = strlen($frase_encriptada);
    for ($i = 0; $i < $longitud; $i++) {
        $letra = $frase_encriptada[$i];
        if (ctype_alpha($letra)) {
            $mayuscula = ctype_upper($letra);
            $letra = strtolower($letra);
            $letra_desencriptada = chr(((ord($letra) - $clave - 97 + 26) % 26) + 97);
            if ($mayuscula) {
                $letra_desencriptada = strtoupper($letra_desencriptada);
            }
            $frase_desencriptada .= $letra_desencriptada;
        } else {
            $frase_desencriptada .= $letra;
        }
    }
    return $frase_desencriptada;
}

$frase_desencriptada = desencriptar($frase_encriptada, 1);

echo "Frase desencriptada:";
echo "$frase_desencriptada";
?>
