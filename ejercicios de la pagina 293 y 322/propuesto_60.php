
<html>
<head>
    <title>numeros mayores de cada columna</title>
</head>
<body>

<?php
$matriz = array(
    array(1, 2, 3),
    array(4, 5, 6),
    array(7, 8, 9),
    array(10, 11, 12)
);

$mayores = array();
for ($j = 0; $j < 3; $j++) {
    $mayor = $matriz[0][$j];
    for ($i = 1; $i < 4; $i++) {
        if ($matriz[$i][$j] > $mayor) {
            $mayor = $matriz[$i][$j];
        }
    }
    $mayores[$j] = $mayor;
}

echo "<h2>Números mayores de cada columna:</h2>";
echo "<table border='1'>";
echo "<tr><th>Columna</th><th>Mayor</th></tr>";
for ($j = 0; $j < 3; $j++) {
    echo "<tr>";
    echo "<td>" . ($j + 1) . "</td>";
    echo "<td>" . $mayores[$j] . "</td>";
    echo "</tr>";
}
echo "</table>";
?>

</body>
</html>
