<?php
  function ordenar_ascendente($numeros) {
    sort($numeros);
    return $numeros;
  }

  function ordenar_descendente($numeros) {
    rsort($numeros);
    return $numeros;
  }

  $numeros = array();

  if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $numeros = $_POST['numeros'];

    $numeros = explode(",", $numeros);

    $orden = $_POST['orden'];

    if ($orden == "A") {
      $numeros = ordenar_ascendente($numeros);
    } elseif ($orden == "D") {
      $numeros = ordenar_descendente($numeros);
    }
  }
?>
<html>
<head>
  <title>Ordenar numeros</title>
</head>
<body>
  <h1>Ordenar numeros</h1>
  <form method="post" action="">
    <label for="numeros">Ingrese los numeros separados por comas:</label>
    <input type="text" id="numeros" name="numeros" value="<?php echo implode(",", $numeros); ?>">
    <br>
    <label for="orden">Seleccione el orden:</label>
    <select id="orden" name="orden">
      <option value="A">Ascendente</option>
      <option value="D">Descendente</option>
    </select>
    <br>
    <button type="submit">Ordenar</button>
  </form>
  <?php if ($_SERVER['REQUEST_METHOD'] === 'POST') { ?>
    <p>Numeros ordenados: <?php echo implode(", ", $numeros); ?></p>
  <?php } ?>
</body>
</html>
