<?php
  if(isset($_POST['submit'])) {
    $numeros = $_POST['numeros'];

    $numeros = explode(",", $numeros);

    $mayor = max($numeros);
    $menor = min($numeros);
  }
?>
<html>
<head>
  <title>Numero mayor y menor</title>
</head>
<body>
  <h1>Numero mayor y menor</h1>
  <form method="POST" action="">
    <label>Ingrese 4 numeros separados por comas:</label>
    <input type="text" name="numeros">
    <button type="submit" name="submit">Calcular</button>
  </form>

  <?php if(isset($_POST['submit'])) { ?>
    <p>Los numeros ingresados son: <?php echo implode(", ", $numeros); ?></p>
    <p>El numero mayor es: <?php echo $mayor; ?></p>
    <p>El numero menor es: <?php echo $menor; ?></p>
  <?php } ?>
</body>
</html>
