<?php
$frase =  readline("");

$palabras = str_word_count($frase, 1);
$repetidas = array_count_values($palabras);

foreach ($repetidas as $palabra => $cantidad) {
    if ($cantidad > 1) {
        echo "$palabra Palabras repetidas: ($cantidad veces)";
    }
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Contar Palabras Repetidas</title>
</head>
<body>
	<h1> Palabras Repetidas</h1>
	<form method="post" action="propuesto_68.php">
		<label for="frase">Frase:</label>
		<input type="text" name="frase" id="frase">
		<input type="submit" value="Contar">
	</form>
</body>
</html>
