
<html>
<head>
	<title>Suma de filas en matriz</title>
</head>
<body>

	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<label for="num1">numero 1:</label>
		<input type="number" name="num1" id="num1"><br>

		<label for="num2">numero 2:</label>
		<input type="number" name="num2" id="num2"><br>

		<label for="num3">numero 3:</label>
		<input type="number" name="num3" id="num3"><br>

		<label for="num4">numero 4:</label>
		<input type="number" name="num4" id="num4"><br>

		<label for="num5">numero 5:</label>
		<input type="number" name="num5" id="num5"><br>

		<label for="num6">numero 6:</label>
		<input type="number" name="num6" id="num6"><br>

		<input type="submit" name="submit" value="Calcular suma de filas">
	</form>

	<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$matriz = array(array($_POST["num1"], $_POST["num2"]), array($_POST["num3"], $_POST["num4"]), array($_POST["num5"], $_POST["num6"]));

		$sumaFila1 = $matriz[0][0] + $matriz[0][1];
		$sumaFila2 = $matriz[1][0] + $matriz[1][1];
		$sumaFila3 = $matriz[2][0] + $matriz[2][1];

		echo "<p>Suma de fila 1: " . $sumaFila1 . "</p>";
		echo "<p>Suma de fila 2: " . $sumaFila2 . "</p>";
		echo "<p>Suma de fila 3: " . $sumaFila3 . "</p>";
	}
	?>

</body>
</html>
