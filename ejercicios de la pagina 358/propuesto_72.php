<html>
<head>
	<title>Promedio de las dos notas mayores</title>
    <style type="text/css">
        body {
           color: blue;
		   text-align: center;
           justify-self: unset;
        }
        input {
            color: blue;
        }
    </style>
</head>
<body>
	<form method="post">
        <h2>Notas </h2>
		<label for="nota1">Nota 1:</label>
		<input type="text" id="nota1" name="nota1"><br><br>

		<label for="nota2">Nota 2:</label>
		<input type="text" id="nota2" name="nota2"><br><br>

		<label for="nota3">Nota 3:</label>
		<input type="text" id="nota3" name="nota3"><br><br>

		<input type="submit" value="Calcular promedio">
	</form>
</body>
</html>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$nota1 = $_POST['nota1'];
	$nota2 = $_POST['nota2'];
	$nota3 = $_POST['nota3'];

	if (!empty($nota1) && !empty($nota2) && !empty($nota3) && is_numeric($nota1) && is_numeric($nota2) && is_numeric($nota3)) {
		$nota1 = (float) $nota1;
		$nota2 = (float) $nota2;
		$nota3 = (float) $nota3;

		$notas = [$nota1, $nota2, $nota3];
		rsort($notas);

		$promedio = ($notas[0] + $notas[1]) / 2;

		echo "El promedio de las dos notas mayores es: " . $promedio;
	}
}
?>
