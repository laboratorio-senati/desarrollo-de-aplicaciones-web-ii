<html>
<head>
	<title>rectángulo</title>
    <style type="text/css">
        form {
           color: blue;
        }
        input {
            color: blue;
        }

    </style>
</head>
<body>
	<form method="post">
        <h2>Area de un rectangulo</h2>
		<label for="base">Ingrese la base del rectangulo:</label>
		<input type="text" id="base" name="base"><p></p>
		<label for="altura">Ingrese la altura del rectangulo:</label>
		<input type="text" id="altura" name="altura"><p></p>

		<input type="submit" value="Calcular area"><p></p>
	</form>
</body>
</html>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$base = $_POST['base'];
	$altura = $_POST['altura'];

	if (is_numeric($base) && is_numeric($altura)) {
		$area = $base * $altura;

		echo "El area del rectangulo es: " . $area;
	}
}
?>
