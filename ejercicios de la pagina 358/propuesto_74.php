<html>
<head>
	<title>Suma</title>
    <style type="text/css">
        form {
           color: blue;
        }
        input {
            color: blue;
        }

    </style>
</head>
<body>
	<form method="post">
        <h2>Suma de dígitos pares e impares</h2>
		<label for="numero">Ingrese un número:</label>
		<input type="text" id="numero" name="numero"><br><br>

		<input type="submit" value="Calcular suma">
	</form>
</body>
</html>
<?php
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$numero = $_POST['numero'];

	if (is_numeric($numero)) {
		$numero_str = (string) $numero;

		$suma_pares = 0;
		$suma_impares = 0;

		for ($i = 0; $i < strlen($numero_str); $i++) {
			$digito = $numero_str[$i];
			if ($digito % 2 == 0) {
				$suma_pares += $digito;
			} else {
				$suma_impares += $digito;
			}
		}

		echo "La suma de los dígitos pares es: " . $suma_pares . "<br>";
		echo "La suma de los dígitos impares es: " . $suma_impares;
	}
}
?>
