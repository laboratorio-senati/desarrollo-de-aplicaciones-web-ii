<html>
<head>
	<title>Document</title>
</head>
<body>
    <h2>Matriz</h2>

<?php
$A = array(array(1, 2),array(3, 4));

$B = array(array(5, 6),array(7, 8));

$C = array(array(0, 0),array(0, 0));

for ($i = 0; $i < 2; $i++) {
    for ($j = 0; $j < 2; $j++) {
        $C[$i][$j] = 0;
        for ($k = 0; $k < 2; $k++) {
            $C[$i][$j] += $A[$i][$k] * $B[$k][$j];
        }
    }
}
echo "<table border='1'>";
echo "<tr><td>Matriz A:</td><td>" . $A[0][0] . "</td><td>" . $A[0][1] . "</td></tr>";
echo "<tr><td></td><td>" . $A[1][0] . "</td><td>" . $A[1][1] . "</td></tr>";
echo "<tr><td>Matriz B:</td><td>" . $B[0][0] . "</td><td>" . $B[0][1] . "</td></tr>";
echo "<tr><td></td><td>" . $B[1][0] . "</td><td>" . $B[1][1] . "</td></tr>";
echo "<tr><td>Matriz C:</td><td>" . $C[0][0] . "</td><td>" . $C[0][1] . "</td></tr>";
echo "<tr><td></td><td>" . $C[1][0] . "</td><td>" . $C[1][1] . "</td></tr>";
echo "</table>";
?>

</body>
</html>
