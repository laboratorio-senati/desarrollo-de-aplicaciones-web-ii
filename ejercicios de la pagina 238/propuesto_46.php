<!DOCTYPE html>
<html>
<head>
  <title>Cantidad de números múltiplos</title>
</head>
<body>
  <?php
  $cifras = $_POST["cifras"];
  $divisor = $_POST["divisor"];
  $minimo = pow(10, $cifras - 1);
  $maximo = pow(10, $cifras) - 1;
  $cantidad = 0;
  for ($i = $minimo; $i <= $maximo; $i++) {
    if ($i % $divisor == 0) {
      $cantidad++;
    }
  }
  echo "La cantidad de números múltiplos de $divisor con $cifras cifras es: " . $cantidad;
  ?>
  <h1>Cantidad de números múltiplos</h1>
  <form method="post" action="verificar.php">
    <label for="cifras">Ingrese la cantidad de cifras:</label>
    <input type="text" id="cifras" name="cifras">
    <br>
    <label for="divisor">Ingrese el divisor:</label>
    <input type="text" id="divisor" name="divisor">
    <br>
    <input type="submit" value="Verificar">
  </form>
</body>
</html>