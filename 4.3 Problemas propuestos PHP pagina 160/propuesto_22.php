<html>
  <head>
    <title>Día de la semana</title>
  </head>
  <body>
    <h2>Introduzca un numero del 1 al 7 para conocer el dia de la semana</h2>
    <form method="GET" action="propuesto_22.php">
      <input type="text" name="num">
      <input type="submit" value="Enviar">
    </form>
    <?php
      if (isset($_GET['num'])) { 
        $num = $_GET['num'];
        $dias_semana = array("domingo", "lunes", "martes", "miercoles", "jueves", "viernes", "sabado");
        $dia_semana = $dias_semana[$num - 1];
        echo "El número " . $num . " corresponde al día de la semana " . $dia_semana ;
      }
    ?>
  </body>
</html>
