<!DOCTYPE html>
<html>
<head>
	<title>document</title>
</head>
<body>
	<form method="post">
		<h2>Mes en letras</h2>
		<label for="mes">ingrese el numero de mes:</label>
		<input type="number" name="mes" id="mes"><br>
		<input type="submit" name="submit" value="Obtener mes">
	</form>
	<?php
	if (isset($_POST['submit'])) {
		$meses = array(
			1 => "Enero",
			2 => "Febrero",
			3 => "Marzo",
			4 => "Abril",
			5 => "Mayo",
			6 => "Junio",
			7 => "Julio",
			8 => "Agosto",
			9 => "Septiembre",
			10 => "Octubre",
			11 => "Noviembre",
			12 => "Diciembre"
		);
		$mes = $_POST['mes'];
		if (array_key_exists($mes, $meses)) {
			echo "El mes es: " . $meses[$mes];
		} else {
			echo "el numero del mes es invalido";
		}
	}
	?>
</body>
</html>