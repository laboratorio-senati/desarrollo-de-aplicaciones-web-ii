<html>
  <head>
    <title>Ciudad a visitar</title>
  </head>
  <body>
    <h2>Calculadora de ciudad a visitar</h2>
    <form method="POST" action="">
      <label for="sexo">Seleccione su sexo:</label>
      <select name="sexo" id="sexo">
        <option value="masculino">Masculino</option>
        <option value="femenino">Femenino</option>
      </select>
      <br><br>
      <label for="puntaje">Ingrese su puntaje:</label>
      <input type="number" name="puntaje" id="puntaje">
      <br><br>
      <input type="submit" value="Calcular">
    </form>
    <?php
      if ($_SERVER['REQUEST_METHOD'] === 'POST') { 
        $sexo = $_POST['sexo'];
        $puntaje = $_POST['puntaje'];
        $ciudad = "";
        if ($sexo == "masculino") {
          if ($puntaje >= 18 && $puntaje <= 35) {
            $ciudad = "Arequipa";
          } else if ($puntaje >= 36 && $puntaje <= 75) {
            $ciudad = "Cuzco";
          } else if ($puntaje > 75) {
            $ciudad = "Iquitos";
          }
        } else if ($sexo == "femenino") {
          if ($puntaje >= 18 && $puntaje <= 35) {
            $ciudad = "Cuzco";
          } else if ($puntaje >= 36 && $puntaje <= 75) {
            $ciudad = "Iquitos";
          } else if ($puntaje > 75) {
            $ciudad = "Arequipa";
          }
        }
        echo "Usted debe visitar " . $ciudad ;
      }
    ?>
  </body>
</html>
