<html>
  <head>
    <title>Document</title>
  </head>
  <body>
    <h2>Calculadora de días restantes en el año</h2>
    <form method="POST" action="">
      <label for="fecha">Introduzca una fecha:</label>
      <input type="date" name="fecha" id="fecha">
      <input type="submit" value="Calcular">
    </form>
    <?php
      if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        $fecha = $_POST['fecha'];
        $anio_actual = date('Y');
        $ultimo_dia_anio_actual = strtotime('31-12-' . $anio_actual);
        $dias_restantes = round(($ultimo_dia_anio_actual - strtotime($fecha)) / (60 * 60 * 24));
        echo "Faltan " . $dias_restantes . " dias para que acabe el año.";
      }
    ?>
  </body>
</html>
