<html>
  <head>
    <title>Estado Civil</title>
  </head>
  <body>
    <h2>Obtener el nombre del estado civil mediante codigo</h2>
    <form method="POST" action="">
      <label for="codigo">Ingrese el código:</label>
      <input type="number" name="codigo" id="codigo">
      <br><br>
      <input type="submit" value="Buscar">
    </form>
    <?php
      if ($_SERVER['REQUEST_METHOD'] === 'POST') { 
        $codigo = $_POST['codigo'];
        $estado_civil = "";
        switch ($codigo) {
          case 0:
            $estado_civil = "Soltero";
            break;
          case 1:
            $estado_civil = "Casado";
            break;
          case 2:
            $estado_civil = "Divorciado";
            break;
          case 3:
            $estado_civil = "Viudo";
            break;
          default:
            $estado_civil = "Código de estado civil no válido";
        }
        echo "El estado civil es: " . $estado_civil;
      }
    ?>
  </body>
</html>
