<html>
<head>
	<title>Document</title>
</head>
<body>
	<form method="post">
        <h2>porsentaje de numeros pares</h2>
		<label for="numeros">Ingrese los numeros separados por coma:</label>
		<input type="text" name="numeros" id="numeros" required>
		<input type="submit" value="Calcular">
	</form>

	<?php
	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		$numeros = explode(',', $_POST['numeros']);
		$num_pares = 0;
		$num_impares = 0;
		$num_neutros = 0;

		foreach ($numeros as $numero) {
			if ($numero == 0) {
				$num_neutros++;
			} elseif ($numero % 2 == 0) {
				$num_pares++;
			} else {
				$num_impares++;
			}
		}

		$total_numeros = count($numeros);
		$porcentaje_pares = ($num_pares / $total_numeros) * 100;
		$porcentaje_impares = ($num_impares / $total_numeros) * 100;
		$porcentaje_neutros = ($num_neutros / $total_numeros) * 100;

		echo "Porcentaje de numeros pares: " . round($porcentaje_pares, 2) . "%";
		echo "Porcentaje de numeros impares: " . round($porcentaje_impares, 2) . "%";
		echo "Porcentaje de numeros neutros: " . round($porcentaje_neutros, 2) . "%";
	}
	?>
</body>
</html>
