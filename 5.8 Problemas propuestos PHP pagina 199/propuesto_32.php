<html>
<head>
	<title>Document</title>
</head>
<body>
	<h2>Cantidad de numeros pares e impares</h2>
	<form method="POST">
		<label>Ingrese el numero inicial:</label>
		<input type="number" name="inicio"><br>
		<label>Ingrese el numero final:</label>
		<input type="number" name="fin"><br>
		<input type="submit" value="Calcular">
	</form>
	<br>
	<?php
		if(isset($_POST['inicio']) && isset($_POST['fin'])) {
			$inicio = $_POST['inicio'];
			$fin = $_POST['fin'];

			$num_pares = 0;
			$num_impares = 0;

			for($i = $inicio; $i <= $fin; $i++) {
				if($i % 5 != 0) {
					if($i % 2 == 0) {
						$num_pares++;
					} else {
						$num_impares++;
					}
				}
			}

			echo "Hay $num_pares números pares y $num_impares números impares en el rango de $inicio a $fin (sin considerar los múltiplos de 5)";
		}
	?>
</body>
</html>
