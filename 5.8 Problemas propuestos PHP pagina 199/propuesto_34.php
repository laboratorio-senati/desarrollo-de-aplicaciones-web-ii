<html>
<head>
	<title>Document</title>
</head>
<body>
	<h2>Cantidad de dígitos</h2>
	<form method="POST">
		<label>Ingrese un numero:</label>
		<input type="number" name="numero"><br>
		<input type="submit" value="Calcular">
	</form>
	<br>
	<?php
		if(isset($_POST['numero'])) {
			$numero = $_POST['numero'];
			$numero_str = strval($numero);
			$num_ceros = 0;
			for($i = 0; $i < strlen($numero_str); $i++) {
				if(substr($numero_str, $i, 1) == "0") {
					$num_ceros++;
				}
			}
			echo "El numero $numero contiene $num_ceros digitos";
		}
	?>
</body>
</html>
