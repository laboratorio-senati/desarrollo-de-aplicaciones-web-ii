<html>
<head>
	<title>Document</title>
</head>
<body>
	<h2>Ingresa dos numeros</h2>
	<form method="post" action="propuesto_40.php">
		<label for="num1">primer numero 1:</label>
		<input type="number" name="num1" id="num1"><br><br>

		<label for="num2">segundo numero 2:</label>
		<input type="number" name="num2" id="num2"><br><br>

		<input type="submit" value="Calcular MCD">
	</form>

	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$num1 = $_POST["num1"];
			$num2 = $_POST["num2"];
			$mcd = 1;
			for ($i = 2; $i <= $num1; $i++) {
				while ($num1 % $i == 0) {
					if ($num2 % $i == 0) {
						$mcd *= $i;
					}

					$num1 /= $i;
				}
			}
			for ($i = 2; $i <= $num2; $i++) {
				while ($num2 % $i == 0) {
					if ($num1 % $i == 0) {
						$mcd *= $i;
					}

					$num2 /= $i;
				}
			}

			echo "El MCD de $num1 y $num2 es: $mcd";
		}
	?>
</body>
</html>
