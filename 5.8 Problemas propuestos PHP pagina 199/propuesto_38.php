<html>
<head>
	<title>Números capicúa en un rango</title>
</head>
<body>
	<?php
		$inicio = 1;
		$fin = 1000;
		$contador = 0;

		for ($i = $inicio; $i <= $fin; $i++) {
			if (esCapicua($i)) {
				$contador++;
			}
		}

		echo "El numero de capicuas en el rango de $inicio a $fin es: $contador";

		function esCapicua($num) {
			return $num == strrev($num);
		}
	?>
</body>
</html>
