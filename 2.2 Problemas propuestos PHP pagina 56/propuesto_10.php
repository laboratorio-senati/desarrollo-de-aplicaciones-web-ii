<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>como convertir grados sexagesimales a centesimales</title>
</head>
<body>
	<h1>conversion de grados sexagesimales a centesimales</h1>
	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<label for="sexagesimales">grados sexagesimales:</label>
		<input type="text" id="sexagesimales" name="sexagesimales"><br><br>
		<input type="submit" value="Convertir">
	</form>

	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$sexagesimales = $_POST["sexagesimales"];

			$centesimales = $sexagesimales * 100 / 90;

			echo "Resultado:";
			echo $sexagesimales . " los grados sexagesimales son equivalentes a " . $centesimales . " grados centesimales.";
		}
	?>
</body>
</html>