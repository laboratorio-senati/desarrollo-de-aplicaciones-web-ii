<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>como allar el area y perimetro de un rectangulo</title>
</head>
<body>
	<h1>area y perimetro de un rectangulo</h1>
	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<label for="base">base:</label>
		<input type="text" id="base" name="base"><br><br>
		<label for="altura">altura:</label>
		<input type="text" id="altura" name="altura"><br><br>
		<input type="submit" value="Calcular">
	</form>

	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$base = $_POST["base"];
			$altura = $_POST["altura"];

			$area = $base * $altura;
			$perimetro = 2 * ($base + $altura);

			echo "Resultados:";
			echo "-Area: " . $area . "<br>";
			echo "-Perímetro: " . $perimetro . "<br>";
		}
	?>
</body>
</html>