<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
	<h1>conversion de milimetros a metros, decimetros, centimetros y milimetros</h1>
	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<label for="milimetros">milimetros:</label>
		<input type="text" id="milimetros" name="milimetros"><br><br>
		<input type="submit" value="Convertir">
	</form>

	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$milimetros = $_POST["milimetros"];

			$metros = floor($milimetros / 1000);
			$decimetros = floor(($milimetros % 1000) / 100);
			$centimetros = floor(($milimetros % 100) / 10);
			$milimetros = $milimetros % 10;

			echo "Resultados:";
			echo $metros . " metros, " . $decimetros . " decimetros, " . $centimetros . " centimetros y " . $milimetros . " milimetros.";
		}
	?>
</body>
</html>