<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
	<h1>conversion de horas a minutos y segundos</h1>
	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<label for="horas">horas:</label>
		<input type="text" id="horas" name="horas"><br><br>
		<input type="submit" value="Convertir">
	</form>

	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$horas = $_POST["horas"];

			$minutos = $horas * 60;
			$segundos = $horas * 3600;

			echo "Resultados:";
			echo $horas . " las horas son equivalentes a " . $minutos . " minutos y " . $segundos . " segundos.";
		}
	?>
</body>
</html>