<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>como allar el area y perimetro de un cuadrado</title>
</head>
<body>
	<h1>area y perimetro de un cuadrado</h1>
	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<label for="lado">longitud del lado:</label>
		<input type="text" id="lado" name="lado"><br><br>
		<input type="submit" value="Calcular">
	</form>

	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$lado = $_POST["lado"];

			$area = $lado * $lado;
			$perimetro = 4 * $lado;

			echo "Resultados:";
			echo "Area: " . $area . "<br>";
			echo "Perímetro: " . $perimetro . "<br>";
		}
	?>
</body>
</html>