<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
	<h1>suma y resta de dos numeros enteros</h1>
	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<label for="num1">numero 1:</label>
		<input type="text" id="num1" name="num1"><br><br>
		<label for="num2">numero 2:</label>
		<input type="text" id="num2" name="num2"><br><br>
		<input type="submit" value="Calcular">
	</form>

	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$num1 = $_POST["num1"];
			$num2 = $_POST["num2"];

			$suma = $num1 + $num2;
			$resta = $num1 - $num2;

			echo "Resultados:";
			echo "Suma: " . $suma . "<br>";
			echo "Resta: " . $resta . "<br>";
		}
	?>
</body>
</html>